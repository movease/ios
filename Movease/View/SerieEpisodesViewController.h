//
//  SerieEpisodesViewController.h
//  Movease
//
//  Created by fofo fofodev on 17/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface SerieEpisodesViewController : UIViewController
{
    @private
    Movie* serie_;
    
}

@property(strong, nonatomic) Movie* serie;

@property (strong, nonatomic) IBOutlet UITableView *episodesTableView;


@end
