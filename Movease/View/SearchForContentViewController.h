//
//  SearchForContentViewController.h
//  Movease
//
//  Created by fofo fofodev on 12/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface SearchForContentViewController : UIViewController
{
    @private
    Movie* serie_;
}

@property(strong, nonatomic) NSMutableArray<Movie*>* episodesList;
@property(strong, nonatomic) IBOutlet UITextField *contentTypeTextField;
@property(strong, nonatomic) IBOutlet UITextField *contentTitleTextField;



@property (strong, nonatomic) IBOutlet UIButton *touchShowEpisodesButton;



@property(strong,nonatomic) Movie* serie;
@property (strong, nonatomic) IBOutlet UILabel *directorLabel;
@property (strong, nonatomic) IBOutlet UILabel *actorsLabel;
@property (weak, nonatomic) IBOutlet UITextView *plotTextView;
@property (strong, nonatomic) IBOutlet UIImageView *serieImageView;

@property (strong, nonatomic) IBOutlet UILabel *serieGenreLabel;
@property (strong, nonatomic) IBOutlet UILabel *serieTotalSeasonsLabel;
@property (strong, nonatomic) IBOutlet UILabel *serieRealeaseDateLabel;
@property (strong, nonatomic) IBOutlet UIButton *addToFavoriteButton;


@end
