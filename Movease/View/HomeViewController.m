//
//  HomeViewController.m
//  Movease
//
//  Created by Alexandre Ménielle on 30/01/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "HomeViewController.h"
#import "MovieTableViewCell.h"
#import "SavedMoviesCollectionViewCell.h"
#import "Movie.h"
#import "SearchForContentViewController.h"
#import "MovieDetailViewController.h"


@interface HomeViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, NSFetchedResultsControllerDelegate, UITextFieldDelegate>
    
@end

@implementation HomeViewController

-(void) viewWillAppear:(BOOL)animated {
    [self viewDidLoad];
}


-(void) touchSearch:(id) sender{
    
    SearchForContentViewController* searchForContent = [[SearchForContentViewController alloc] init];
    
    [self.navigationController pushViewController:searchForContent animated:YES];
}


-(instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    
    if(self!=nil){
        
        UIBarButtonItem* searchItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(touchSearch:)];
        searchItem.tintColor = [UIColor whiteColor];
        

        self.title = @"Accueil";

        
        self.navigationItem.rightBarButtonItem = searchItem;
        self.automaticallyAdjustsScrollViewInsets = false;
        
        movies_ = [[NSMutableArray alloc] init];
        proposalsPage_ = 1;
        
        NSString* strURL = [NSString stringWithFormat:@"https://www.omdbapi.com/?s=%c&page=%i",arc4random_uniform(26) + 'a', proposalsPage_];
        [self getProposals:[NSURL URLWithString:strURL]];
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    savedMovies_ = [[NSMutableArray alloc] init];
    savedSeries_ = [[NSMutableArray alloc] init];
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_background"] forBarMetrics:UIBarMetricsDefault];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor whiteColor], NSForegroundColorAttributeName,
                                                shadow, NSShadowAttributeName,
                                                [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil]];

    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.proposalTableView registerNib:[UINib nibWithNibName:@"MovieTableViewCell" bundle:nil] forCellReuseIdentifier:KcellId];
    
    [self.savedMoviesCollectionView registerNib:[UINib nibWithNibName:@"SavedMoviesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:KSavedMovieId];
    
    [self.savedSeriesCollectionView registerNib:[UINib nibWithNibName:@"SavedMoviesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:KSavedMovieId];
    
    [self initFooterView];
    
    //Load tableview with movies in CoreData
    NSManagedObjectContext *context = [self managedObjectContext];
    
    [context setStalenessInterval:0.0];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Movies"];
    NSMutableArray* tmpMovies = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (NSManagedObject* obj in tmpMovies) {
        Movie* movie = [Movie new];
        movie.id = [obj valueForKey:@"id"];
        movie.year = [obj valueForKey:@"year"];
        movie.title = [obj valueForKey:@"title"];
        movie.type = [obj valueForKey:@"type"];
        movie.poster = [obj valueForKey:@"poster"];
        
        if([movie.type isEqualToString:@"movie"]){
            [savedMovies_ addObject:movie];
        }
        if([movie.type isEqualToString:@"series"]){
            [savedSeries_ addObject:movie];
        }
    }
    
    [self.savedMoviesCollectionView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onMoviesLongPress:)]];
    [self.savedSeriesCollectionView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onSeriesLongPress:)]];
    [self.proposalTableView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onProposalsLongPress:)]];

    [self.savedMoviesCollectionView reloadData];
    [self.savedSeriesCollectionView reloadData];
    [self.proposalTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getProposals : (NSURL*) url{
    dispatch_queue_t queue = dispatch_queue_create("proposal_queue", NULL);
    dispatch_async(queue, ^{
        
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:url];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if(error){
                NSLog(@"%@",error);
                return;
            }
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            
            //parse le json pour remplir tableview
            NSArray *results = [json objectForKey:@"Search"];
            
            for (NSDictionary *dic in results) {
                Movie* movie = [[Movie alloc] init];
                movie.id = [dic valueForKey:@"imdbID"];
                movie.title = [dic valueForKey:@"Title"];
                movie.year = [dic valueForKey:@"Year"];
                movie.type = [dic valueForKey:@"Type"];
                movie.poster = [UIImage imageNamed:@"default-movie"];
                
                dispatch_queue_t queue = dispatch_queue_create("image_queue", NULL);
                dispatch_async(queue, ^{
                    UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dic valueForKey:@"Poster"]]]];
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        if(image != nil)
                            movie.poster = image;
                        [self.proposalTableView reloadData];
                        self.proposalTableView.tableFooterView = nil;
                    });
                });
                [movies_ addObject:movie];
            }
        }];
        [dataTask resume];
    });
}

//CollectionViewDelegat and CollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (savedSeries_.count > 0){
        self.savedSeriesLabel.hidden = true;
    }else{
        self.savedSeriesLabel.hidden = false;
    }
    if (savedMovies_.count > 0){
        self.savedMoviesLabel.hidden = true;
    }else{
        self.savedMoviesLabel.hidden = false;
    }
    if (collectionView == self.savedMoviesCollectionView)
        return savedMovies_.count;
    if (collectionView == self.savedSeriesCollectionView)
        return savedSeries_.count;
    
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(125, 145);
}

static NSString* const KSavedMovieId=@"SavedMoviesCell";

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SavedMoviesCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:KSavedMovieId forIndexPath:indexPath];
    
    if (collectionView == self.savedMoviesCollectionView)
        [cell setup:[savedMovies_ objectAtIndex:indexPath.row]];
    if (collectionView == self.savedSeriesCollectionView)
        [cell setup:[savedSeries_ objectAtIndex:indexPath.row]];


    return cell;

}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Movie* movie = nil;
    
    if (collectionView == self.savedMoviesCollectionView)
        movie = [savedMovies_ objectAtIndex:indexPath.row];
    if (collectionView == self.savedSeriesCollectionView)
        movie = [savedSeries_ objectAtIndex:indexPath.row];
    
    MovieDetailViewController* detailsVC = [[MovieDetailViewController alloc] initWithMovie:movie];
    
    [self.navigationController pushViewController:detailsVC animated:YES];
}
//END - CollectionViewDelegate and CollectionViewDataSource

-(void)onMoviesLongPress:(UILongPressGestureRecognizer*)pGesture{
    
    if (pGesture.state == UIGestureRecognizerStateBegan){
        
        NSIndexPath *indexPath = [self.savedMoviesCollectionView indexPathForItemAtPoint:[pGesture locationInView:self.savedMoviesCollectionView]];
        SavedMoviesCollectionViewCell *cell = [self.savedMoviesCollectionView cellForItemAtIndexPath:indexPath];
        
        [self uiAlertController:@"Voulez-vous supprimer le film des favoris ou voir le détail ?"
                       andText1:@"Détail" withBlock1:^(UIAlertAction *action) {
                           //block which catch event
                           MovieDetailViewController* detailsVC = [[MovieDetailViewController alloc] initWithMovie:cell.movieCell];
                           [self.navigationController pushViewController:detailsVC animated:YES];
                       } andText2:@"Supprimer" withBlock2:^(UIAlertAction *action) {
                           //block which catch event
                           [cell.movieCell delete:self];
                           [savedMovies_ removeObject:cell.movieCell];
                           [self.savedMoviesCollectionView reloadData];
                           [self.proposalTableView reloadData];
                       } andWithMovie:cell.movieCell];
        
    }
}

-(void)onSeriesLongPress:(UILongPressGestureRecognizer*)pGesture{
    if (pGesture.state == UIGestureRecognizerStateBegan){
        NSIndexPath *indexPath = [self.savedSeriesCollectionView indexPathForItemAtPoint:[pGesture locationInView:self.savedSeriesCollectionView]];
        SavedMoviesCollectionViewCell *cell = [self.savedSeriesCollectionView cellForItemAtIndexPath:indexPath];
        
        [self uiAlertController:@"Voulez-vous supprimer la série des favoris ou voir le détail ?"
                       andText1:@"Détail" withBlock1:^(UIAlertAction *action) {
                           //block which catch event
                           MovieDetailViewController* detailsVC = [[MovieDetailViewController alloc] initWithMovie:cell.movieCell];
                           [self.navigationController pushViewController:detailsVC animated:YES];
                       } andText2:@"Supprimer" withBlock2:^(UIAlertAction *action) {
                           //block which catch event
                           [cell.movieCell delete:self];
                           [savedSeries_ removeObject:cell.movieCell];
                           [self.savedSeriesCollectionView reloadData];
                           [self.proposalTableView reloadData];
                       } andWithMovie:cell.movieCell];
        
    }
}

-(void)onProposalsLongPress:(UILongPressGestureRecognizer*)pGesture{
    if (pGesture.state == UIGestureRecognizerStateBegan){
        NSIndexPath *indexPath = [self.proposalTableView indexPathForRowAtPoint:[pGesture locationInView:self.proposalTableView]];
        MovieTableViewCell *cell = [self.proposalTableView cellForRowAtIndexPath:indexPath];
        
        if([cell.movieCell isSaved]){
            [self uiAlertController:@"Voulez-vous le supprimer des favoris ou voir le détail ?"
                           andText1:@"Détail" withBlock1:^(UIAlertAction *action) {
                               //block which catch event
                               MovieDetailViewController* detailsVC = [[MovieDetailViewController alloc] initWithMovie:cell.movieCell];
                               [self.navigationController pushViewController:detailsVC animated:YES];
                           } andText2:@"Supprimer" withBlock2:^(UIAlertAction *action) {
                               //block which catch event
                               [cell.movieCell delete:self];
                               if ([cell.movieCell.type isEqualToString:@"movie"]) {
                                   [savedMovies_ removeObject:[self findMovieWithId:cell.movieCell.id]];
                                   [self.savedMoviesCollectionView reloadData];
                               } else {
                                   [savedSeries_ removeObject:[self findMovieWithId:cell.movieCell.id]];
                                   [self.savedSeriesCollectionView reloadData];
                               }
                               [self.proposalTableView reloadData];
                           } andWithMovie:cell.movieCell];
        }else{
            [self uiAlertController:@"Voulez-vous l'ajouter aux favoris ou voir le détail ?"
                           andText1:@"Détail" withBlock1:^(UIAlertAction *action) {
                               //block which catch event
                               MovieDetailViewController* detailsVC = [[MovieDetailViewController alloc] initWithMovie:cell.movieCell];
                               [self.navigationController pushViewController:detailsVC animated:YES];
                           } andText2:@"Favoris" withBlock2:^(UIAlertAction *action) {
                               //block which catch event
                               [self addFavorite:cell.movieCell];
                           } andWithMovie:cell.movieCell];
        }
    }
}

//Custom uiAlertController Method
- (void) uiAlertController:(NSString*)message andText1:(NSString*) text1 withBlock1:(void (^)(UIAlertAction *action))block1 andText2:(NSString*)text2 withBlock2:(void (^)(UIAlertAction *action))block2 andWithMovie:(Movie*) movie{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:movie.title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:text1
                                                        style:UIAlertActionStyleDefault
                                                      handler:block1]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:text2
                                                        style:UIAlertActionStyleDestructive
                                                      handler:block2]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

//TableViewDelegate and TableViewDataSource
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    MovieDetailViewController* detailsVC = [[MovieDetailViewController alloc] initWithMovie: [movies_ objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:detailsVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (movies_.count > 0){
        self.proposalActivityIndicator.hidden = true;
        [self.proposalActivityIndicator stopAnimating];
    }
    else {
        [self.proposalActivityIndicator startAnimating];
        self.proposalActivityIndicator.hidden = false;
    }
    return movies_.count;
}

static NSString* const KcellId=@"movieCell";

- (Movie*) findMovieWithId:(NSString*)id {
    for (Movie* movie in savedMovies_) {
        if ([movie.id isEqualToString:id]) {
            return movie;
        }
    }
    
    for (Movie* movie in savedSeries_) {
        if ([movie.id isEqualToString:id]) {
            return movie;
        }
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MovieTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:KcellId];
    
    if(cell == nil){
        cell = [[MovieTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:KcellId];
    }
    
    Movie* movie = [movies_ objectAtIndex:indexPath.row];
    [cell setup:movie];
    
    if ([movie isSaved]) {
        [cell.add setImage:[UIImage imageNamed:@"remove-heart"] forState:UIControlStateNormal];
    } else {
        [cell.add setImage:[UIImage imageNamed:@"add-heart"] forState:UIControlStateNormal];
    }
    
    cell.add.tag = indexPath.row;
    [cell.add addTarget:self action:@selector(addToFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
//END - TablViewDelegate and TableViewDataSource

- (void) addToFavorite:(UIButton*) sender{
    Movie* movie = [movies_ objectAtIndex:sender.tag];
    [self addFavorite:movie];
}

- (void) addFavorite:(Movie*) movie{
    if([movie isSaved]){
        [movie delete:self];
        if ([movie.type isEqualToString:@"movie"]) {
            [savedMovies_ removeObject:movie];
            [self.savedMoviesCollectionView reloadData];
        } else {
            [savedSeries_ removeObject:movie];
            [self.savedSeriesCollectionView reloadData];
        }
    }else{
        NSString* toastType = @"séries";
        if ([movie.type isEqualToString:@"movie"]) {
            [savedMovies_ addObject:movie];
            [self.savedMoviesCollectionView reloadData];
            toastType = @"films";
        } else {
            [savedSeries_ addObject:movie];
            [self.savedSeriesCollectionView reloadData];
        }
        [movie save: self];
    }
    
    [self.proposalTableView reloadData];
}

//Footer (loader)
-(void)initFooterView
{
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width/2, 40.0)];
    
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    actInd.tag = 10;
    actInd.frame = CGRectMake(self.view.frame.size.width/2, 15, 0, 0);
    actInd.hidesWhenStopped = YES;
    
    [footerView addSubview:actInd];
    
    actInd = nil;
}

//Scroll event to load more movies
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.proposalActivityIndicator.isHidden){
        BOOL endOfTable = (scrollView.contentOffset.y >= ((movies_.count * 195) - scrollView.frame.size.height)); //195 is row height
        
        if (endOfTable && !scrollView.dragging && !scrollView.decelerating)
        {
            self.proposalTableView.tableFooterView = footerView;
            
            [(UIActivityIndicatorView *)[footerView viewWithTag:10] startAnimating];
        }
    }
    
    NSString* strURL = [NSString stringWithFormat:@"https://www.omdbapi.com/?s=%c&page=%i",arc4random_uniform(26) + 'a', ++proposalsPage_];
    [self getProposals:[NSURL URLWithString:strURL]];
        
}

//Core Data methods
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
