//
//  ContentDetailViewController.h
//  TP_Ios_API
//
//  Created by fofo fofodev on 17/01/2017.
//  Copyright © 2017 PROJECT SEMAINE C. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface MovieDetailViewController : UIViewController<UIScrollViewDelegate>

@property (strong, nonatomic) Movie* movie;
@property (strong, nonatomic) IBOutlet UIImageView *ContentImg;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;
@property (strong, nonatomic) IBOutlet UILabel *infosLabel;
@property (strong, nonatomic) IBOutlet UILabel *synopsisLabel;
@property (strong, nonatomic) IBOutlet UILabel *actorsLabel;
@property (strong, nonatomic) IBOutlet UILabel *realisatorsLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerInfos;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerSynopsis;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerActors;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerRealisators;


- (instancetype) initWithMovie:(Movie*)movie;

@end
