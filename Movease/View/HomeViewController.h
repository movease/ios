//
//  HomeViewController.h
//  Movease
//
//  Created by Alexandre Ménielle on 30/01/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Movie.h"


@interface HomeViewController : UIViewController{
    NSMutableArray* movies_;
    NSMutableArray* savedMovies_;
    NSMutableArray* savedSeries_;
    int proposalsPage_;
    UIView* footerView;
    
}



@property (weak, nonatomic) IBOutlet UITableView *proposalTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *savedMoviesCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *savedSeriesCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *savedMoviesLabel;
@property (weak, nonatomic) IBOutlet UILabel *savedSeriesLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *proposalActivityIndicator;

@end
