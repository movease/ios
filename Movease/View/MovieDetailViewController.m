//
//  ContentDetailViewController.m
//  TP_Ios_API
//
//  Created by fofo fofodev on 17/01/2017.
//  Copyright © 2017 PROJECT SEMAINE C. All rights reserved.
//

#import "MovieDetailViewController.h"
#import "Movie.h"

@interface MovieDetailViewController ()

@end

@implementation MovieDetailViewController

@synthesize movie;
@synthesize saveBtn;
@synthesize deleteBtn;
@synthesize infosLabel;
@synthesize synopsisLabel;
@synthesize actorsLabel;
@synthesize realisatorsLabel;
@synthesize contentScrollView;
@synthesize imageScrollView;
@synthesize spinnerInfos;
@synthesize spinnerSynopsis;
@synthesize spinnerActors;
@synthesize spinnerRealisators;

- (instancetype) initWithMovie:(Movie*)mov {
    self = [super init];
    
    if (self != nil) {
        self.movie = mov;
    }
    
    return self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.contentScrollView) {
        [self.imageScrollView setContentOffset:CGPointMake(self.contentScrollView.contentOffset.x, self.contentScrollView.contentOffset.y * 0.25) animated:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentScrollView.delegate = self;
    
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.contentScrollView.contentSize = self.contentScrollView.frame.size;
    self.contentScrollView.frame = self.view.frame;
    [self.view addSubview:self.contentScrollView];
    
    if ([movie isSaved]) {
        deleteBtn.hidden = NO;
        saveBtn.hidden = YES;
    } else {
        saveBtn.hidden = NO;
        deleteBtn.hidden = YES;
    }
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60) forBarMetrics:UIBarMetricsDefault];
    
    if (movie.title)
        self.titleLabel.text = movie.title;
    if (movie.poster) {
        if([movie.poster isKindOfClass:[NSData class]]){
            self.ContentImg.image = [UIImage imageWithData:movie.poster];
        }else
            self.ContentImg.image = movie.poster;
        
        [self.ContentImg.layer setMasksToBounds:YES];
    }
    if (movie.released)
        self.infosLabel.text = movie.released;
    if (movie.actors)
        self.actorsLabel.text = movie.actors;
    if (movie.plot)
        self.synopsisLabel.text = movie.plot;
    if (movie.director)
        self.realisatorsLabel.text = movie.director;
    
    [self getMovieDetails];
}

- (IBAction)saveBtnClick:(UIButton *)sender {
    [movie save:self];
    
    saveBtn.hidden = YES;
    deleteBtn.hidden = NO;
}

- (IBAction)deleteBtnClick:(UIButton *)sender {
    [movie delete:self];
    
    saveBtn.hidden = NO;
    deleteBtn.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getMovieDetails {
    NSString* strURL = [NSString stringWithFormat:@"https://www.omdbapi.com/?i=%@&plot=full", movie.id];
    
    dispatch_queue_t queue = dispatch_queue_create("movie_details_queue", NULL);
    dispatch_async(queue, ^{
        
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:strURL]];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if(error){
                NSLog(@"%@",error);
                return;
            }
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            movie.id = [json valueForKey:@"imdbID"];
            movie.title = [json valueForKey:@"Title"];
            movie.year = [json valueForKey:@"Year"];
            movie.type = [json valueForKey:@"Type"];
            movie.rated = [json valueForKey:@"Rated"];
            movie.released = [json valueForKey:@"Released"];
            movie.runtime = [json valueForKey:@"Runtime"];
            movie.genre = [json valueForKey:@"Genre"];
            movie.director = [json valueForKey:@"Director"];
            movie.writer = [json valueForKey:@"Writer"];
            movie.actors = [json valueForKey:@"Actors"];
            movie.plot = [json valueForKey:@"Plot"];
            movie.language = [json valueForKey:@"Language"];
            movie.country = [json valueForKey:@"Country"];
            movie.awards = [json valueForKey:@"Awards"];
            movie.metascore = [json valueForKey:@"Metascore"];
            movie.imdbRating = [json valueForKey:@"imdbRating"];
            movie.imdbVotes = [json valueForKey:@"imdbVotes"];
            movie.totalSeasons = [[json valueForKey:@"totalSeasons"] intValue];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.infosLabel.text = [NSString stringWithFormat:@"Sortie: %@\rDurée: %@\rGenre: %@\rNote imdb: %@", movie.released, movie.runtime, movie.genre, movie.imdbRating];
                self.actorsLabel.text = movie.actors;
                self.synopsisLabel.text = movie.plot;
                self.realisatorsLabel.text = movie.director;
                [self.spinnerInfos stopAnimating];
                [self.spinnerSynopsis stopAnimating];
                [self.spinnerActors stopAnimating];
                [self.spinnerRealisators stopAnimating];
            });
        }];
        [dataTask resume];
    });
}

@end
