//
//  MovieTableViewCell.m
//  Movease
//
//  Created by Alexandre Ménielle on 30/01/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import "MovieTableViewCell.h"
#import "Movie.h"

@implementation MovieTableViewCell

@synthesize title;
@synthesize img;
@synthesize year;
@synthesize type;
@synthesize add;
@synthesize movieCell;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setup:(Movie *) movie {
    movieCell = movie;
    self.title.text = movie.title;
    self.year.text  = movie.year;
    
    [self.img.layer setCornerRadius:7];
    
    if([movie.poster isKindOfClass:[NSData class]]){
        self.img.image = [UIImage imageWithData:movie.poster];
    }
    else
        self.img.image = movie.poster;
    
    self.type.text = movie.type;
    [self.img.layer setMasksToBounds:YES];
}



@end
