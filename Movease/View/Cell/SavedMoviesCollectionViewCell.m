//
//  SavedMoviesCollectionViewCell.m
//  Movease
//
//  Created by Alexandre Ménielle on 11/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import "SavedMoviesCollectionViewCell.h"


@implementation SavedMoviesCollectionViewCell

@synthesize img;
@synthesize titleLabel;
@synthesize movieCell;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setup:(Movie *) movie {
    self.movieCell = movie;
    
    self.titleLabel.text = movie.title;
    
    [self.img.layer setCornerRadius:7.0];
    
    if([movie.poster isKindOfClass:[NSData class]]){
        self.img.image = [UIImage imageWithData:movie.poster];
    }else
        self.img.image = movie.poster;
    
    [self.img.layer setMasksToBounds:YES];
}

@end
