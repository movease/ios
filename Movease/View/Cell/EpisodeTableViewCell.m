//
//  EpisodeTableViewCell.m
//  Movease
//
//  Created by fofo fofodev on 18/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import "EpisodeTableViewCell.h"

@implementation EpisodeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setEpisode:(Movie *)episode
{
    
    self.episodeImageView.image = episode.poster;
    self.episodePlotLabel.text  = episode.plot;
    self.episodeYearLabel.text =  episode.year;
    self.episodeTitleLable.text = episode.title;
    
    [self.episodeImageView.layer setMasksToBounds:YES];
    [self.episodeImageView.layer setCornerRadius:10.0];
}
@end
