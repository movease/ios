//
//  SavedMoviesCollectionViewCell.h
//  Movease
//
//  Created by Alexandre Ménielle on 11/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface SavedMoviesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) Movie *movieCell;

- (void) setup:(Movie *) movie;

@end
