//
//  MovieTableViewCell.h
//  Movease
//
//  Created by Alexandre Ménielle on 30/01/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface MovieTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UIButton *add;
@property (weak, nonatomic) Movie *movieCell;


- (void) setup:(Movie *) movie;

@end
