//
//  EpisodeTableViewCell.h
//  Movease
//
//  Created by fofo fofodev on 18/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface EpisodeTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *episodeImageView;
@property (strong, nonatomic) IBOutlet UILabel *episodeTitleLable;

@property (strong, nonatomic) IBOutlet UILabel *episodeYearLabel;

@property (strong, nonatomic) IBOutlet UILabel *episodePlotLabel;


-(void) setEpisode:(Movie*) episode;


@end
