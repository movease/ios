//
//  SerieEpisodesViewController.m
//  Movease
//
//  Created by fofo fofodev on 17/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import "SerieEpisodesViewController.h"
#import "MovieTableViewCell.h"
#import "EpisodeTableViewCell.h"

@interface SerieEpisodesViewController ()
<UITableViewDelegate, UITableViewDataSource>



@end

@implementation SerieEpisodesViewController

@synthesize serie = serie_;


NSString* const searchViewCellId = @"EpisodeTableViewCell";



-(void) getEpisodesFullInfo{
    
    dispatch_queue_t queue = dispatch_queue_create("episodes_details_queue", NULL);
    for(NSInteger i = 0; i < [self.serie.episodes count]; i++){
        Movie* episode = [self.serie.episodes objectAtIndex:i];
        
        NSString* urlStr = [NSString stringWithFormat:@"http://www.omdbapi.com/?i=%@", episode.id];
        urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        NSURL *url = [NSURL URLWithString:urlStr];

        dispatch_async(queue, ^{
            
            NSURLRequest* request = [[NSURLRequest alloc] initWithURL:url];
            NSURLSession* session = [NSURLSession sharedSession];
            
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if(error){
                    NSLog(@"\n\n\nsomething went wrong with the request for %@. Error is %@\n\n\n",url,error);
                    return;
                }
                NSDictionary* resp = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                
                
                if([[resp objectForKey:@"Response"] isEqualToString:@"True"]){
                    
                    dispatch_queue_t queue = dispatch_queue_create("episodes_images_queu", NULL);
                    
                    dispatch_async(queue, ^{
                    
                        episode.poster = [UIImage imageNamed:@"default-movie"];
                        
                        episode.imgUrl = [resp objectForKey:@"Poster"];
                        
                        episode.year = [resp objectForKey:@"Year"];
                        episode.plot = [resp objectForKey:@"Plot"];
                        
                        UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:episode.imgUrl]]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(image != nil){
                                episode.poster = image;
                                [self.episodesTableView reloadData];
                            }
                        });
                    });
                }
            }];
            [dataTask resume];
        });
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EpisodeTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:searchViewCellId];
    
    
    if(cell == nil){
        cell = [[EpisodeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchViewCellId];
    }
    Movie* movie = [self.serie.episodes objectAtIndex:indexPath.row];
    
    [cell setEpisode:movie];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.serie.episodes count];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.episodesTableView.delegate = self;
    self.episodesTableView.dataSource = self;
    
    self.title = [NSString stringWithFormat:@"%@ - saison %ld", self.serie.title, self.serie.totalSeasons];
    
    
    [self getEpisodesFullInfo];
    
    
    // Do any additional setup after loading the view from its nib.
    
    UINib* nib = [UINib nibWithNibName:@"EpisodeTableViewCell" bundle:nil];
    [self.episodesTableView registerNib:nib forCellReuseIdentifier:searchViewCellId];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
