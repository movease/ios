//
//  SearchForContentViewController.m
//  Movease
//
//  Created by fofo fofodev on 12/02/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

//RETREIVE SEASONS FIRST BEFORE RETRIEVING EPISODES BY SEASONS
//TABLEVIEW AND COLLECTIONVIEW INIDE THE TABLEVIEW

#import <CoreData/CoreData.h>
#import "SearchForContentViewController.h"
#import "MovieTableViewCell.h"
#import "Movie.h"
#import "SavedMoviesCollectionViewCell.h"
#import "SerieEpisodesViewController.h"
#import "MovieDetailViewController.h"

@interface SearchForContentViewController ()
< UITextFieldDelegate>

@end

@implementation SearchForContentViewController
@synthesize serie = serie_;


- (IBAction)touchSaveButton:(UIButton*)sender {
    
    if([sender.titleLabel.text isEqualToString:@"Ajouter à mes favoris"]){
        if(![self.serie isSaved]){
            [self.serie save:self];
            [self.addToFavoriteButton setTitle:@"Retirer de mes favoris" forState:UIControlStateNormal];
        }
        
    }
    else{
        [self.serie delete:self];
        [self.addToFavoriteButton setTitle:@"Ajouter à mes favoris" forState:UIControlStateNormal];
    }
}

- (IBAction)touchShowSerieEpisodes:(UIButton*)sender {
    
    if([sender.titleLabel.text isEqualToString:@"voir les détails du film"]){
        MovieDetailViewController* detailedMovie = [[MovieDetailViewController alloc] init];
        detailedMovie.movie =  self.serie ;
        [self.navigationController pushViewController:detailedMovie animated:YES];
    }
    else{
        
        SerieEpisodesViewController* serieEpisodesVC = [[SerieEpisodesViewController alloc] init];
        serieEpisodesVC.serie = self.serie;
        [self.navigationController pushViewController:serieEpisodesVC animated:YES];
    }
    
}

- (IBAction)touchSearchAction:(id)sender {
    NSString* searchTitle = self.contentTitleTextField.text;
    if(searchTitle.length > 0){
        [self getRawSerieInfos:searchTitle];
    }
}



-(void) setSerieHeader:(Movie*) aSerie{
    
    self.serie = aSerie;
    
    self.directorLabel.text = self.serie.director;
    self.actorsLabel.text = self.serie.actors;
    self.plotTextView.text = self.serie.plot;
    self.serieImageView.image = self.serie.poster;
    self.serieGenreLabel.text = self.serie.genre;
    self.serieTotalSeasonsLabel.text = [NSString stringWithFormat:@"%ld\nsaisons", self.serie.totalSeasons];
    self.serieTotalSeasonsLabel.layer.cornerRadius = self.serieTotalSeasonsLabel.frame.size.width / 2;
    self.serieRealeaseDateLabel.text = self.serie.released;
}


-(void) retrieveEpisodesIds{
    
    NSString* urlString = [NSString stringWithFormat:@"http://www.omdbapi.com/?i=%@&Season=%ld", self.serie.id,self.serie.totalSeasons];
    
    NSURL* url = [NSURL URLWithString:urlString];
    
    
    dispatch_queue_t queue = dispatch_queue_create("episodes_queue", NULL);
    dispatch_async(queue, ^{
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:url];
        NSURLSession* session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if(error){
                NSLog(@"\n\n\nsomething went wrong with the request for %@. Error is %@\n\n\n",urlString,error);
                return;
            }
            NSDictionary* resp = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            
            if([[resp objectForKey:@"Response"] isEqualToString:@"True"]){
                
                dispatch_queue_t queue = dispatch_queue_create("episode_queue", NULL);
                
                dispatch_async(queue, ^{
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        for (NSDictionary* subDictionary in [resp objectForKey:@"Episodes"]) {
                            
                            Movie* anEpisode = [[Movie alloc] init];
                            
                            anEpisode.id = [subDictionary objectForKey:@"imdbID"];
                            anEpisode.title = [subDictionary objectForKey:@"Title"];
                        
                            [self.serie.episodes addObject:anEpisode];
                        }
                    });
                });
            }
        }];
        [dataTask resume];
    });
}


-(NSString*) pickASerieName{
    NSArray* newest = @[@"The Sopranos",@"V",@"Divorce",@"The Crown",@"Game of Thrones",@"Girls",@"Parks and Recreation",@"New Girl"];
    return newest[arc4random_uniform((u_int32_t)newest.count)];
}

-(void) getRawSerieInfos:(NSString*) aSerieName{
    aSerieName = [aSerieName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    aSerieName = [NSString stringWithFormat:@"http://www.omdbapi.com/?t=%@&y=&plot=full&r=json", aSerieName];
    aSerieName = [aSerieName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSURL*  url = [NSURL URLWithString:aSerieName];

    dispatch_queue_t queue = dispatch_queue_create("suggestion_queue", NULL);
    dispatch_async(queue, ^{
        
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:url];
        NSURLSession* session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if(error){
                NSLog(@"\n\n\nsomething went wrong with the request for %@. Error is %@\n\n\n",aSerieName,error);
                return;
            }
            NSDictionary* resp = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            
            if([[resp objectForKey:@"Response"] isEqualToString:@"True"]){
                
                Movie* serie = [[Movie alloc] init];
                serie.id = [resp objectForKey:@"imdbID"];
                
                serie.totalSeasons =[[resp objectForKey:@"totalSeasons"] integerValue];
                serie.title = [resp objectForKey:@"Title"];
                serie.type =[resp objectForKey:@"Type"];
                serie.genre = [resp objectForKey:@"Genre"];
                serie.director = [resp objectForKey:@"Writer"];
                serie.actors = [resp objectForKey:@"Actors"];
                serie.year = [resp objectForKey:@"Year"];
                serie.plot = [resp objectForKey:@"Plot"];
                serie.released = [resp objectForKey:@"Released"];
                
                serie.poster = [UIImage imageNamed:@"default-movie"];
                
                
                dispatch_queue_t queue = dispatch_queue_create("image_queue", NULL);
                dispatch_async(queue, ^{
                    
                    
                    serie.imgUrl =[resp objectForKey:@"Poster"];
                    
                    UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:serie.imgUrl]]];
                    dispatch_async(dispatch_get_main_queue(), ^{
            
                        
                        //self.episodesList = [[NSMutableArray alloc]init];
                        
                        if(image != nil){
                            serie.poster = image;
                        }
                        [self setSerieHeader:serie];
                    
                        if([serie isSaved]){
                            [self.addToFavoriteButton setTitle:@"Retirer de mes favoris" forState:UIControlStateNormal];
                        }
                        
                        if([serie.type isEqualToString:@"Movie"]){
                            [self.touchShowEpisodesButton setTitle:@"voir les détails du film" forState:UIControlStateNormal];
                        }
                        
                        
                        self.serie.episodes = [[NSMutableArray alloc] init];
                        [self retrieveEpisodesIds];
                    });
                });
            }
        }];
        [dataTask resume];
    });
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self != nil){
        
        self.title = @"Recherche";
        
        [self getRawSerieInfos:[self pickASerieName]];
        
        [self.serieImageView.layer setMasksToBounds:YES];
        [self.serieImageView.layer setCornerRadius:25.0];
        [self.serieImageView.layer setBorderWidth:2.5];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@""];
    

    [self.serieImageView setImage:[UIImage imageNamed:@""]];
    // Do any additional setup after loading the view from its nib.
    
    
    self.contentTitleTextField.delegate = self;
    self.contentTitleTextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
