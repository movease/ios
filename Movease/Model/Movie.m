//
//  Movie.m
//  Movease
//
//  Created by Alexandre Ménielle on 30/01/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Movie.h"

@implementation Movie

@synthesize id = id_;
@synthesize title = title_;
@synthesize year = year_;
@synthesize type = type_;
@synthesize poster = poster_;
@synthesize rated = rated_;
@synthesize released = released_;
@synthesize runtime = runtime_;
@synthesize genre = genre_;
@synthesize director = director_;
@synthesize writer = writer_;
@synthesize actors = actors_;
@synthesize plot = plot_;
@synthesize language = language_;
@synthesize country = country_;
@synthesize awards = awards_;
@synthesize imdbRating = imdbRating_;
@synthesize imdbVotes = imdbVotes_;
@synthesize totalSeasons = totalSeasons_;
@synthesize episodes = episodes_;
@synthesize imgUrl = imgUrl_;

//Core Data methods
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) saveInContext {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

- (bool) isSaved {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest* req = [NSFetchRequest new];
    [req setEntity:[NSEntityDescription entityForName:@"Movies" inManagedObjectContext:context]];
    [req setPredicate:[NSPredicate predicateWithFormat:@"(id == %@)", self.id]];
    
    NSError* error = nil;
    NSArray* results = [NSArray new];
    results = [context executeFetchRequest:req error:&error];
    
    return [results count];
}

- (void) save:(UIViewController*)vc {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *newMovie = [NSEntityDescription insertNewObjectForEntityForName:@"Movies" inManagedObjectContext:context];
    [newMovie setValue:self.id forKey:@"id"];
    [newMovie setValue:self.title forKey:@"title"];
    [newMovie setValue:self.type forKey:@"type"];
    [newMovie setValue:self.year forKey:@"year"];
    [newMovie setValue:UIImageJPEGRepresentation(self.poster, 0) forKey:@"poster"];
    
    [self saveInContext];
    
    NSString* toastType = @"séries";
    
    if ([self.type isEqualToString:@"movie"]) {
        toastType = @"films";
    }
    
    [self toast:[NSString stringWithFormat:@"%@ a été ajouté à vos %@.", self.title, toastType] delay:1.0 view:vc];
}

- (void) delete:(UIViewController*)vc {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest* req = [NSFetchRequest new];
    [req setEntity:[NSEntityDescription entityForName:@"Movies" inManagedObjectContext:context]];
    [req setPredicate:[NSPredicate predicateWithFormat:@"id == %@", self.id]];
    
    NSError* error = nil;
    NSArray* results = [context executeFetchRequest:req error:&error];
    
    if (error != nil) {
        [self toast:@"Une erreur est survenue lors de la suppression." delay:1.0 view:vc];
        return;
    }
    
    for (NSManagedObject* movie in results) {
        [context deleteObject:movie];
    }
    
    NSString* toastType = @"séries";
    
    if ([self.type isEqualToString:@"movie"]) {
        toastType = @"films";
    }
    
    [self toast:[NSString stringWithFormat:@"%@ a été supprimé de vos %@.", self.title, toastType] delay:1.0 view:vc];
    
    [self saveInContext];
}

- (void)toast:(NSString*)message delay:(unsigned long long)delay view:(UIViewController*)vc {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [vc presentViewController:alert animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}



@end
