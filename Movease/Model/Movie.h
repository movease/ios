//
//  Movie.h
//  Movease
//
//  Created by Alexandre Ménielle on 30/01/2017.
//  Copyright © 2017 Alexandre Ménielle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Movie : NSObject

@property(strong,nonatomic) NSString* id;
@property(strong,nonatomic) NSString* title;
@property(strong,nonatomic) NSString* year;
@property(strong,nonatomic) NSString* type;
@property(strong,nonatomic) NSString* rated;
@property(strong,nonatomic) NSString* released;
@property(strong,nonatomic) NSString* runtime;
@property(strong,nonatomic) NSString* genre;
@property(strong,nonatomic) NSString* director;
@property(strong,nonatomic) NSString* writer;
@property(strong,nonatomic) NSString* actors;
@property(strong,nonatomic) NSString* plot;
@property(strong,nonatomic) NSString* language;
@property(strong,nonatomic) NSString* country;
@property(strong,nonatomic) NSString* awards;
@property(strong,nonatomic) NSString* metascore;
@property(strong,nonatomic) NSNumber* imdbRating;
@property(strong,nonatomic) NSString* imdbVotes;
@property(strong,nonatomic) UIImage* poster;
@property(nonatomic) NSInteger totalSeasons;
@property(strong,nonatomic) NSMutableArray<Movie*>* episodes;
@property(strong,nonatomic) NSString* imgUrl;

- (void)save:(UIViewController*)vc;
- (void)delete:(UIViewController*)vc;
- (bool)isSaved;
@end
