# PAGE ACCUEIL #
* Carousel derniers films sauvegardés
* onHold ⇒ menu contextuel (supprimer, détails)
* onClick ⇒ détails
* Carousel dernières séries sauvegardées
* onHold ⇒ menu contextuel (supprimer, détails)
* onClick ⇒ détails

* Suggestions de films aléatoires (verticale)
* Lettre random aléatoire ⇒ http://www.omdbapi.com/?s=x
* onClick ⇒ page détails
* onHold ⇒ menu contextuel (ajouter/supprimer, détails)
* Load items onScrolling 10 par 10 (page=1, page=2,…)

# PAGE RECHERCHE #
* Recherche par titre
* Bouton recherche

* Un résultat unique :
* Poster
* Titre word-wrap si nécessaire
* Année
* Acteurs
* Réalisateurs
* Bouton save
* Bouton delete (si déjà enregistré)
* Bouton voir épisodes si type == "series"

# PAGE DETAILS #
* Titre
* Appel API ⇒ http://www.omdbapi.com/?i=id&plot=full
* Année
* Durée (runtime)
* Genre
* Réalisateur
* Acteurs
* Plot
* Poster
* imdbRating
* Bouton save
* Bouton delete
* parallax poster (?)
* Bande-annonce (?)